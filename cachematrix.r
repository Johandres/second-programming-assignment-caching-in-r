## creates a special "matrix" object that can cache its inverse.
makeCacheMatrix <- function(x = numeric()) {
        inv <- NULL
        get <- function() x
        getInverse <- function() {
                if(length(x)%%sqrt(length(x))==0) { ## make sure matrix is square
                        inv <<- solve(x)
                }
        }
        list(get = get, getInverse = getInverse)
}